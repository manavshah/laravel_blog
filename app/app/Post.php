<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class post extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'title',
        'category_id',
        'excerpt',
        'content',
        'image',
        'published_at'
    ];
    /**
     * HELPER FUNCTION
     */
    public function deleteImage()
    {
        Storage::delete($this->image);
    }
}