<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Http\Requests\Posts\CreatePostRequest;
use App\Http\Requests\Posts\UpdatePostRequest;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::paginate(10);
        return view('posts.index',compact([
            'posts'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('posts.create', compact(['categories']));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Image Upload and stoore the name of image
        $image = $request->file('image')->store('posts');
        //run commmand : php artisan storage:link
        //create Post
        //dd($request);
        Post::create([
                'title'=>$request->title,
                'category_id'=>$request->category_id,
                'excerpt'=>$request->excerpt,
                'content'=>$request->content,
                'image'=>$image,
                'published_at'=>$request->published_at
        ]);
        //session storage
        session()->flash('success','Post Created Successfully');
        //redirect
        return redirect(route('posts.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('posts.edit')->withPost($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, Post $post)
    {
        $data = $request->only(['title','excerpt','content','published_at']);
        if($request->hasFile('image')){
            $image = $request->image->store('posts');
            $post->deleteImage();
            $data['image'] = $image;
        }
        $post->update($data);
        session()->flash('success','Post Updated successfully');
        return redirect(route('posts.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    /**
     * Deletes the post Permanently
     */
    public function destroy($id)
    {
        $post = Post::onlyTrashed()->findOrFail($id);
        $post->deleteImage();
        $post->forceDelete();
        session()->flash('success','Post deleted successfully');
        return redirect()->back();
    }
    /**
     * Soft Deletes $post
     */
    public function trash(Post $post)
    {
        $post->delete();
        session()->flash('success','Post trashed successfully');
        return redirect(route('posts.index'));
    }
    public function trashed()
    {
        //$trashed = DB::table('posts')->whereNotNull('deleted_at');
        $trashed = Post::onlyTrashed()->paginate(10);
        return view('posts.trashed')->with('posts',$trashed);
    }
    public function restore($id)
    {
        $trashedPost = Post::onlyTrashed()->findOrFail($id);
        $trashedPost->restore();
        session()->flash('success','Post restored successfully');
        return redirect(route('posts.index'));
    }
}
