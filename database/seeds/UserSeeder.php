<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::where('email','sshweta@gmail.com')->get()->first();
        if(!$user){
            \App\User::create([
                'name'=>'Manav Shah',
                'email'=>'manavshah140@gmail.com',
                'password'=>\Illuminate\Support\Facades\Hash::make('manav123'),
                'role'=>'admin'
            ]);
        }else{
            $user->update(['role'=>'admin']);
        }

        \App\User::create([
            'name'=>'Vidhi Parikh',
            'email'=>'vidhi@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('vidhi123')
        ]);

        \App\User::create([
            'name'=>'Yukta Peswani',
            'email'=>'yukta@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('yukta123')
        ]);
    }
}
